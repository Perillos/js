console.log('hola');

const countries = [
    {title: 'Random title0', imgUrl: 'https://picsum.photos/300/200?random=1'},
    {title: 'Random title1', imgUrl: 'https://picsum.photos/300/200?random=2'},
    {title: 'Random title2', imgUrl: 'https://picsum.photos/300/200?random=3'},
    {title: 'Random title3', imgUrl: 'https://picsum.photos/300/200?random=4'},
    {title: 'Random title4', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

/* 
Crear un div
Bucle que por cada elemento
    crear un h4
    crear un img

*/

/* Creamos el div */

const myDivBig = document.createElement('div');
document.body.appendChild(myDivBig)
/* Bucle que crea h4 y img */
for (const countrie of countries) {
    let myH4 = document.createElement('h4');
    myH4.textContent = countrie.title // inserta el texto
    let myImg = document.createElement('img');
    myImg.src = countrie.imgUrl // inserta src
    const myDivSmall = document.createElement('div'); // crea un div auxiliar
    const buttonSmall = document.createElement('button'); // crea un boton
    buttonSmall.textContent = 'Deleter'
    myDivSmall.appendChild(myH4)
    myDivSmall.appendChild(myImg)
    myDivBig.appendChild(myDivSmall)
    myDivBig.appendChild(buttonSmall)

    
    /* Elimina el texto y la imagen */
    // function deltering() {  
    //     myDivSmall.remove()
    // }
    // buttonSmall.addEventListener('click', deltering)
    function deltering() {  
        console.log(myDivSmall.style.visibility);
        myDivSmall.style.visibility == 'hidden'
        ? myDivSmall.style.visibility = 'visible'
        : myDivSmall.style.visibility = 'hidden'
    }
    buttonSmall.addEventListener('click', deltering)

}

// const sexyButton = document.querySelector('button');

// function deltering() {    myDivSmall.removeAttribute('visibility')
//     let lastDiv = myDivBig.lastChild
//     lastDiv.remove()
// }

// sexyButton.addEventListener('click', deltering)


