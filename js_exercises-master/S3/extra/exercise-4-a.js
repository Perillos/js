console.log('hola');

const countries = [
    {title: 'Random title0', imgUrl: 'https://picsum.photos/300/200?random=1'},
    {title: 'Random title1', imgUrl: 'https://picsum.photos/300/200?random=2'},
    {title: 'Random title2', imgUrl: 'https://picsum.photos/300/200?random=3'},
    {title: 'Random title3', imgUrl: 'https://picsum.photos/300/200?random=4'},
    {title: 'Random title4', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

/* 
Crear un div
Bucle que por cada elemento
    crear un h4
    crear un img

*/

/* Creamos el div */

const myDiv = document.createElement('div');
document.body.appendChild(myDiv)
/* Bucle que crea h4 y img */
for (const countrie of countries) {
    let myH4 = document.createElement('h4');
    myH4.textContent = countrie.title // inserta el texto
    console.log(myH4);
    let myImg = document.createElement('img');
    myImg.src = countrie.imgUrl // inserta src
    console.log(myImg);
    myDiv.appendChild(myH4)
    myDiv.appendChild(myImg)
}


