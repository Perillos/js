const apps = [
    'Facebook',
    'Netflix',
    'Instagram',
    'Snapchat',
    'Twitter'
];
const uncontableList = document.createElement('ul')

for (const app of apps) {
    let listItem = document.createElement('li')
    listItem.innerText = app
    uncontableList.appendChild(listItem)
}

document.body.appendChild(uncontableList)