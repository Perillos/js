
/* Seleccionamos el elemento */
const myLitelFocus = document.querySelector('input[type=text]')

console.log(myLitelFocus);
/* Generamos funcion de evento */
const superEvent = function () {
    console.log(myLitelFocus.value);
}
/* Hacemos el focus */
myLitelFocus.addEventListener('focusout', superEvent)

// const myInput = document.querySelector('input');

// myInput.addEventListener('focus', function(e){
//     console.log(myInput.value);
// })
