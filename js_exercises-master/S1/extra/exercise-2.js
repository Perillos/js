const fruits = ['Strawberry', 'Banana', 'Orange', 'Apple'];
const foodSchedule = [
    {name: "Salad", isVegan: true},
    {name: "Salmon", isVegan: false}, 
    {name: "Tofu", isVegan: true}, 
    {name: "Burger", isVegan: false}, 
    {name: "Rice", isVegan: true}, 
    {name: "Pasta", isVegan: true}
];

for (let i = 0; i < foodSchedule.length; i++) {
    let food = foodSchedule[i]
    if (food.isVegan == false) {
        food.name = fruits.shift()
        console.log(food);
        food.isVegan = true;
    }
    
}
console.log(foodSchedule);


foodSchedule.forEach((food) => {
    if (food.isVegan == false) {
      //food.name = fruits.shift();

      food.name = fruits.pop();
      
      // console.log(fruits)
      food.isVegan = true;
    }
  });
  
  console.log(foodSchedule);
  